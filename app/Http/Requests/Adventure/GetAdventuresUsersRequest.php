<?php

namespace App\Http\Requests\Adventure;

use Illuminate\Foundation\Http\FormRequest;

class GetAdventuresUsersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adventure' => 'required|exists:adventures,id',
        ];
    }

    /**
     * Use route parameters for validation
     * @return array
     */
    protected function validationData()
    {
        return $this->route()->parameters();
    }
}