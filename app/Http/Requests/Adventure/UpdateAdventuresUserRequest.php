<?php

namespace App\Http\Requests\Adventure;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Adventure\Adventure;
use Illuminate\Support\Facades\Auth;

class UpdateAdventuresUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $adventure = $this->route('adventure');
        return $adventure && Auth::user()->can('update', $adventure);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adventure' => 'required|exists:adventures,id',
            'user' => 'required|exists:users,id',              
        ];
    }

    /**
     * Use route parameters for validation
     * @return array
     */
    protected function validationData()
    {
        return $this->route()->parameters();
    }    
}
