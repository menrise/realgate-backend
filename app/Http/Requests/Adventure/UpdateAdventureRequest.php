<?php

namespace App\Http\Requests\Adventure;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Adventure\Adventure;
use Illuminate\Support\Facades\Auth;

class UpdateAdventureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $adventure = $this->route('adventure');
        return $adventure && Auth::user()->can('update', $adventure);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'adventureTypeId' => 'required|exists:adventure_types,id',
            'spendTypeId' => 'required|exists:adventure_spends,id',
            'transferTypeId' => 'required|exists:adventure_transfers,id',
            'relationTypeId' => 'required|exists:adventure_relations,id',
            'adventureName' => 'required|regex:/^.{5,35}$/u',
            'adventureDescription' => 'nullable|string|max:150',
            'adventureTime' => 'nullable|date',
            'timeByAgreement' => 'required|boolean',
            'latitude' => 'required|numeric|min:-300|max:300',
            'longitude' => 'required|numeric|min:-300|max:300',
            'needMansCount' => 'required|numeric|min:0|max:10',
            'needWomansCount' => 'required|numeric|min:0|max:10',
            'ageStart' => 'required|numeric|min:18|max:99',
            'ageFinish' => 'required|numeric|min:18|max:99|gt:ageStart',              
        ];
    }
}
