<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sex' => 'required|min:1|max:2',
            'sex_need' => 'required|min:1|max:2',
            'birth_date' => 'required|date',
            'user_group' => 'required|min:1|max:5',
            'phone_number' => 'nullable|regex:/^\+[0-9]{11}$/',
            'name' => 'required|regex:/^[a-zA-Zа-яА-ЯёЁ0-9 -]{2,30}$/u',
        ];
    }
}
