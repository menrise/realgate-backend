<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetMeetsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'composite_meet_ids' => 'String',
            'google_place_ids' => 'String',
        ];
    }
}
