<?php
namespace App\Http\Controllers\Page;

use App\Http\Controllers\Controller;


class PageController extends Controller
{
    /**
     * Display static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function welcome()
    {   
        return view('welcome');
    }

    /**
     * Display static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function license()
    {   
        return view('license');
    }

    /**
     * Display static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function privacy_policy()
    {   
        return view('privacy-policy');
    }    

    /**
     * Display static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function rules()
    {   
        return view('rules');
    }   

   
    /**
     * Display static page.
     *
     * @return \Illuminate\Http\Response
     */
    public function contacts()
    {   
        return view('contacts');
    }       
}
