<?php
namespace App\Http\Controllers\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Chat\PostPrivateMessageRequest;
use App\Http\Requests\Chat\SetPrivateMessageReadedRequest;
use App\Http\Requests\Chat\GetPrivateMessagesRequest;
use App\Http\Requests\Chat\GetContactsRequest;
use App\Models\User;
use App\Models\Chat\PrivateMessage;
use App\Models\Chat\PrivateMessagesContact;
use \App\Events\PrivateMessagesUpdate;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use App\Notifications\ReceivedPrivateMessageNotification;


class PrivateMessageController extends Controller
{

    public function postPrivateMessage(PostPrivateMessageRequest $request, $receiverUserId)
    {
        try {
            $auth_user = Auth::user();
            $receiver_user = User::find($receiverUserId);

            $privateMessageModel = new PrivateMessage;
            $privateMessageModel->sender_id = $auth_user->id;
            $privateMessageModel->receiver_id = $receiver_user->id;
            $privateMessageModel->message = $request->message;
            $privateMessageModel->save();

            $privateMessagesContactModel = new PrivateMessagesContact;
            $privateMessagesContactModel->createOrUpdateContact(
                $auth_user->id, $receiver_user->id, $privateMessageModel->id
            );

            broadcast( new \App\Events\PrivateMessagesUpdate($auth_user->id, $receiverUserId) );

            \Notification::send( [$receiver_user], new ReceivedPrivateMessageNotification($privateMessageModel, $receiver_user) );

            return response()->json('Сообщение отправлено', 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
            
    }

    public function setPrivateMessageReaded(SetPrivateMessageReadedRequest $request, $messageId)
    {
        try {

            $privateMessageModel = new PrivateMessage;
            $privateMessageModel->setMessageReaded($messageId);

            $privateMessage = PrivateMessage::find($messageId);

            $privateMessagesContactModel = new PrivateMessagesContact;
            $privateMessagesContactModel->setMessagesReaded($privateMessage->sender_id, $privateMessage->receiver_id);            
            
            return response()->json('Сообщение прочитано', 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
            
    }

    public function getPrivateMessages(GetPrivateMessagesRequest $request, $secondUserId)
    {
        try {
            $auth_user = Auth::user();
            $secondUser = User::find($secondUserId);

            $privateMessage = new PrivateMessage;
            $messages = $privateMessage->getMessages($auth_user, $secondUser);

            return response()->json($messages, 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
            
    }

    public function getContacts(GetContactsRequest $request)
    {
        try {
            $auth_user = Auth::user();

            $privateMessagesContactModel = new PrivateMessagesContact;
            $contacts = $privateMessagesContactModel->getContacts($auth_user);

            return response()->json($contacts, 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
            
    }   
}
