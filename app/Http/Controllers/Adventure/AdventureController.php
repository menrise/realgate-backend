<?php
namespace App\Http\Controllers\Adventure;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Adventure\CreateAdventureRequest;
use App\Http\Requests\Adventure\UpdateAdventureRequest;
use App\Http\Requests\Adventure\GetNearAdventuresRequest;
use App\Http\Requests\Adventure\GetAdventuresUsersRequest;
use App\Http\Requests\Adventure\CreateAdventuresUserRequest;
use App\Http\Requests\Adventure\UpdateAdventuresUserRequest;
use App\Http\Requests\Adventure\DeleteAdventuresUserRequest;
use App\Http\Requests\Adventure\ApproveUsersRequest;
use App\Models\Adventure\Adventure;
use App\Models\User;
use App\Notifications\AdventureOwnerGetCandidateNotification;
use App\Notifications\AdventureApproveNotification;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class AdventureController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getNearAdventures(GetNearAdventuresRequest $request, Adventure $adventure)
    {   
        
        try {
            $validated = $request->validated();        

            $auth_user = Auth::user();
            if(!$auth_user->id) throw new Exception('Have not auth user id');

            $adventures = $adventure::near($validated['latitude'], $validated['longitude'])
                ->fresh()
                ->with('adventureType')
                ->with('owner')
                ->with('members')
                ->take(100)
                ->get();

            return response()->json($adventures, 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyOwnAdventures(Adventure $adventure)
    {   
        try {
            $auth_user = Auth::user();
            $adventures = $auth_user->adventuresOwnerFresh()
                ->with('adventureType')
                ->with('owner')
                ->with('members')
                ->get();

            return response()->json($adventures, 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getMyParticipationAdventures()
    {   
        try {
            
            $auth_user = Auth::user();
            $adventures = $auth_user->adventuresMemberFresh()
                ->with('adventureType')
                ->with('owner')
                ->with('members')
                ->get();
                
            return response()->json($adventures, 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdventureRequest $request, Adventure $adventure)
    {
        $validated = $request->validated();
        
        try {

            $validated['adventureDescription'] = ($validated['adventureDescription'])
                ? $validated['adventureDescription']
                : '';


            $adventure->create([
                'owner_id' => Auth::user()->id,
                'adventure_type_id' => $validated['adventureTypeId'],
                'adventure_spend_id' => $validated['spendTypeId'],
                'adventure_transfer_id' => $validated['transferTypeId'],
                'adventure_relation_id' => $validated['relationTypeId'], 
                'name' => $validated['adventureName'],
                'description' =>  $validated['adventureDescription'],
                'time' => $validated['adventureTime'],
                'time_by_agreement' => $validated['timeByAgreement'],
                'latitude' => $validated['latitude'],
                'longitude' => $validated['longitude'],
                'need_mans_count' => $validated['needMansCount'],
                'need_womans_count' => $validated['needWomansCount'],      
                'age_start' => $validated['ageStart'],
                'age_finish' => $validated['ageFinish'],                           
            ]);
            
            return response()->json('Приключение создано', 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdventureRequest $request, Adventure $adventure)
    {
        $validated = $request->validated();
        
        try {

            $validated['adventureDescription'] = ($validated['adventureDescription'])
                ? $validated['adventureDescription']
                : '';

            $adventure->fill([
                'owner_id' => Auth::user()->id,
                'adventure_type_id' => $validated['adventureTypeId'],
                'adventure_spend_id' => $validated['spendTypeId'],
                'adventure_transfer_id' => $validated['transferTypeId'],
                'adventure_relation_id' => $validated['relationTypeId'], 
                'name' => $validated['adventureName'],
                'description' => $validated['adventureDescription'],
                'time' => $validated['adventureTime'],
                'time_by_agreement' => $validated['timeByAgreement'],
                'latitude' => $validated['latitude'],
                'longitude' => $validated['longitude'],
                'need_mans_count' => $validated['needMansCount'],
                'need_womans_count' => $validated['needWomansCount'],  
                'age_start' => $validated['ageStart'],
                'age_finish' => $validated['ageFinish'],                                  
            ]);

            $adventure->save();
            
            return response()->json('Приключение обновлено', 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }

    public function getAdventuresUsers(GetAdventuresUsersRequest $request, Adventure $adventure)
    {
        try {
            $adventure_members = $adventure->members()->get();
            return response()->json($adventure_members, 200);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }

    public function postAdventuresUser(CreateAdventuresUserRequest $request, Adventure $adventure)
    {
        try {
            $auth_user = Auth::user();

            if($adventure->owner_id === $auth_user->id){
                return response()->json('Создатель приключения не может добавить сам себя кандидатом', 400);
            }

            $adventureOwner = User::find($adventure->owner_id);
            
            if($auth_user->sex === 1){

                $candidatesMansCount = $adventure->members()
                    ->wherePivot('sex', 1)
                    ->count();  

                if($adventure->need_mans_count * config('app.adventure_candidates_multiplicator') > $candidatesMansCount){
                    $adventure->members()->attach($auth_user->id, ['sex' => $auth_user->sex]);
                    \Notification::send( [$adventureOwner], 
                        new AdventureOwnerGetCandidateNotification($auth_user) 
                    );
                    return response()->json('Кандидат в приключение добавлен', 200);
                }          

            }else if($auth_user->sex === 2){

                $candidatesWomansCount = $adventure->members()
                    ->wherePivot('sex', 2)
                    ->count();        

                if($adventure->need_womans_count * config('app.adventure_candidates_multiplicator') > $candidatesWomansCount){
                    $adventure->members()->attach($auth_user->id, ['sex' => $auth_user->sex]);
                    \Notification::send( [$adventureOwner], 
                        new AdventureOwnerGetCandidateNotification($auth_user) 
                    );                    
                    return response()->json('Кандидат в приключение добавлен', 200);
                }                          
            }else{
                response()->json('Не выбран пол пользователя который хочет стать кандидатом в приключение', 406);
            }
            
            return response()->json('Закончились места кандидатов на это приключение', 423);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
            
    }

    public function deleteAdventuresUser(DeleteAdventuresUserRequest $request, Adventure $adventure, User $user)
    {
        try {
            $auth_user = Auth::user();

            if($adventure->owner_id === $auth_user->id){
                return response()->json('Создатель приключения не может удалить себя из приключения', 400);
            }

            if($auth_user->id === $user->id){
                $adventure->members()->detach($user->id);
                return response()->json('Участник удалён', 200);                
            }else{
                return response()->json('У вас нет прав на удаление участника приключения', 403); 
            }
            
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }   

    public function approveUsers(ApproveUsersRequest $request, Adventure $adventure)
    {
        try {
            $usersWithApproveStatus = [];
            $usersArrValIsId = [];
            $usersArrValIsAprroveIndexIsId = [];
            foreach ($request->usersWithApproveStatus as $key => $value) {
                $usersArrValIsId[] = $value['id'];
                $usersArrValIsAprroveIndexIsId[$value['id']] = $value['approved'];

                if($value['approved']){
                    $usersWithApproveStatus[$value['id']] = 
                    ['approve_membering' => true, 'approve_membering_at' => \Carbon\Carbon::now()];
                }else{
                    $usersWithApproveStatus[$value['id']] = 
                    ['approve_membering' => false, 'approve_membering_at' => \Carbon\Carbon::now()];
                }                
            }
            $adventure->members()->syncWithoutDetaching($usersWithApproveStatus);       

            $usersForApprove = User::whereIn('id', $usersArrValIsId)->get();
            \Notification::send( $usersForApprove, 
                new AdventureApproveNotification($adventure, $usersArrValIsAprroveIndexIsId) 
            );   

            return response()->json('Статусы кандидатов изменены', 200);
  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }      

}
