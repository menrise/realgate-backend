<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\SocialAccount;
use App\Models\User;
use App\Models\ProfileImage;
use Illuminate\Auth\Events\Registered as RegisteredEvent;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UpdateUserDeviceTimeRequest;
use App\Http\Requests\UpdateUserCoordinatesRequest;
use App\Http\Requests\GetUserRequest;
use App\Http\Requests\Image\SetMainProfileImageRequest;
use App\Http\Requests\Image\DeleteProfileImageRequest;
use App\Http\Requests\Image\GetProfileImageRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Storage;

class SocialiteController extends Controller
{
    use RedirectsUsers;

    /**
     * upload profile image
     *
     */
    public function uploadProfileImage(Request $request)
    {       

        try {

            request()->validate([
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,webp|max:30048',
            ]);

            $auth_user = Auth::user();
            
            $profileImagesCount = ProfileImage::where('user_id', $auth_user->id)->count();

            if($profileImagesCount > config('app.limit_profile_images')){
                return response()->json('Достигнут лимит фотографий профиля.', 406);
            }

                       
            $imageName = uniqid().'.'.request()->image->getClientOriginalExtension();
            $image = Image::make(request()->image->getRealPath());

            $pixel_ratio = 3;
            $w = $image->width();
            $h = $image->height();
            if($w > $h) {

                $image->resize(600 * $pixel_ratio, null, function ($constraint) use ($image, $imageName) {
                    $constraint->aspectRatio();
                    //$image->save(public_path('images') . config('app.profile_big_image_path') . $imageName);
                });

                $resource = $image->stream('jpg', 80)->detach();
                $path = Storage::disk('yandex')->put(
                    config('app.profile_big_image_path') . $imageName,
                    $resource
                );


                $image->resize(105 * $pixel_ratio, null, function ($constraint) use ($image, $imageName) {
                    $constraint->aspectRatio();
                    //$image->save(public_path('images') . config('app.profile_small_image_path') . $imageName);
                });      

                $resource = $image->stream()->detach();
                $path = Storage::disk('yandex')->put(
                    config('app.profile_small_image_path') . $imageName,
                    $resource
                );                          
            } else {

                $image->resize(800 * $pixel_ratio, null, function ($constraint) use ($image, $imageName) {
                    $constraint->aspectRatio();
                    //$image->save(public_path('images') . config('app.profile_big_image_path') . $imageName);
                });

                $resource = $image->stream('jpg', 80)->detach();
                $path = Storage::disk('yandex')->put(
                    config('app.profile_big_image_path') . $imageName,
                    $resource
                );


                $image->resize(140 * $pixel_ratio, null, function ($constraint) use ($image, $imageName) {
                    $constraint->aspectRatio();
                    //$image->save(public_path('images') . config('app.profile_small_image_path') . $imageName);
                });

                $resource = $image->stream()->detach();
                $path = Storage::disk('yandex')->put(
                    config('app.profile_small_image_path') . $imageName,
                    $resource
                );                
            }   




            
            $profileImage = new ProfileImage;

            if(empty($auth_user->avatar_file)){
                $auth_user->avatar_file = $imageName;
                $auth_user->save();

                $profileImage->is_main = true;
            }

            $profileImage->user_id = $auth_user->id;
            $profileImage->file_name = $imageName;
            $profileImage->save();



            return response()->json('Фото успешно загружено', 200);  
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }

    public function getProfileImages(GetProfileImageRequest $request)
    {       

        try {
            if($request->userId AND $request->userId > 1){
                $userId = $request->userId;
            }else{
                $auth_user = Auth::user();
                $userId = $auth_user->id;
            }
            
            $profileImages = ProfileImage::where('user_id', $userId)->orderBy('id', 'DESC')->get();

            return response()->json($profileImages, 200);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }

    public function setMainProfileImage(SetMainProfileImageRequest $request)
    {       

        try {

            $auth_user = Auth::user();
            ProfileImage::where('user_id', $auth_user->id)
                ->where('is_main', 1)
                ->update(['is_main' => 0]);       
                     
            ProfileImage::where('user_id', $auth_user->id)
                ->where('id', $request->imageId)
                ->update(['is_main' => 1]);

            $main_image = ProfileImage::where('user_id', $auth_user->id)
                ->where('id', $request->imageId)->first();

            $auth_user->avatar_file = $main_image->file_name;
            $auth_user->save();

            return response()->json('Image was set as main', 200);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }

    public function deleteProfileImage(DeleteProfileImageRequest $request)
    {       

        try {

            $auth_user = Auth::user();

            $image = ProfileImage::where('user_id', $auth_user->id)
                ->where('id', $request->imageId)->first();

            ProfileImage::where('user_id', $auth_user->id)
                ->where('id', $request->imageId)
                ->delete();

            if($image->is_main === 1){

                $imageForSetMain = ProfileImage::where('user_id', $auth_user->id)
                    ->first();

                if($imageForSetMain){

                    $auth_user->avatar_file = $imageForSetMain->file_name;
                    $auth_user->save();    

                    $imageForSetMain->is_main = 1;
                    $imageForSetMain->save();         
                }else{
                    $auth_user->avatar_file = null;
                    $auth_user->save();                        
                }              
            }


            Storage::disk('yandex')->delete(
                config('app.profile_big_image_path') . $image->file_name
            );

            Storage::disk('yandex')->delete(
                config('app.profile_small_image_path') . $image->file_name
            );


            return response()->json('Image deleted', 200);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }


    /**
     * Redirect the user to the Provider authentication page.
     *
     * @param $provider String
     * @return mixed
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->stateless()->redirect();
    }

    /**
     * Obtain the user information from Provider.
     *
     * @param $provider string
     * @throws \Exception
     * @throws \Throwable     
     * @return \Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallback($provider, Request $request)
    {

        try {
            $providerUser = Socialite::driver($provider)->stateless()->user();
        } catch (\Throwable | \Exception $e) {

            if (config('app.debug')) throw $e;
            \Log::error($e);

            return view('redirect-to-app', [
                'deep_link' => $request->input('state')
                . '?error=Невозможно авторизоваться. Пожалуйста попробуйте позже.'
            ]);
        }

        try {

            DB::beginTransaction();
            $user = $this->findOrCreateUser($provider, $providerUser);

            $server_needs_device_time = 0;
            if(!$user->device_time){
                $server_needs_device_time = 1;
            }

            try {
                $token = Auth::login($user, true);
            } catch (JWTException $e) {

                if (config('app.debug')) throw $e;
                \Log::error($e);
                DB::rollBack();

                // something went wrong whilst attempting to encode the token
                return view('redirect-to-app', [
                    'deep_link' => $request->input('state')
                    . '?error=Ошибка авторизации. Пожалуйста попробуйте позже.'
                ]);
            }

            // This session variable can help to determine if user is logged-in via socialite
            session()->put([
                'auth.social_id' => $providerUser->getId()
            ]);

            DB::commit();

        } catch (JWTException $e) {
            
            if (config('app.debug')) throw $e;
            \Log::error($e); // something went wrong whilst create user

            return view('redirect-to-app', [
                'deep_link' => $request->input('state')
                . '?error=Ошибка БД. Пожалуйста попробуйте позже.'
            ]);

        }

        $deep_link = $request->input('state')
            .'?token='.$token.'&sex='.$user->sex.'&sex_need='.$user->sex_need
            .'&birth_date='.$user->birth_date.'&user_group='.$user->user_group
            .'&phone_number='.$user->phone_number.'&user_name='.$user->name.'&server_needs_device_time='.$server_needs_device_time;

        return view('redirect-to-app', ['deep_link' => $deep_link]);
    }

    /**
     * Create a user if does not exist
     *
     * @param $providerName string
     * @param $providerUser
     * @return mixed
     */
    protected function findOrCreateUser($providerName, $providerUser)
    {
        $social = SocialAccount::firstOrNew([
            'provider_user_id' => $providerUser->getId(),
            'provider' => $providerName
        ]);
        if ($social->exists) {
            return $social->user;
        }

        $user = User::create([
            'name' => $providerUser->getName(),
            'email' => $providerUser->getEmail(),
            'avatar' => $providerUser->getAvatar(),
            'password' => bcrypt(str_random(30)),
        ]);
        
        $social->user()->associate($user);
        $social->save();

        event(new RegisteredEvent($user));

        return $user;
    }


    /**
     * Update user
     *
     * @param $request UpdateUserRequest
     * @return json
     */
    public function updateUser(UpdateUserRequest $request)
    {
        
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('JWT error', 401); 
        }
       
       $validated = $request->validated();         

        try {

            $result = $user->update($validated);

            if($result){
                return response()->json('Обновлено', 200);  
            }else{
                return response()->json('Невозможно обновить', 406);
            }

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }

    /**
     * Update user device time
     *
     * @param $request UpdateUserDeviceTimeRequest
     * @return json
     */
    public function updateUserDeviceTime(UpdateUserDeviceTimeRequest $request)
    {
        
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('JWT error', 401); 
        }
       
       $validated = $request->validated();      
       $validated['device_time_at_server_time'] = \Carbon\Carbon::now();

        try {

            $result = $user->update($validated);

            if($result){
                return response()->json('Обновлено', 200);  
            }else{
                return response()->json('Невозможно обновить', 406);
            }

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }

    /**
     * Update user Coordinates
     *
     * @param $request UpdateUserCoordinateRequest
     * @return json
     */
    public function updateUserCoordinates(UpdateUserCoordinatesRequest $request)
    {
        
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('JWT error', 401); 
        }
       
       $validated = $request->validated();      

        try {

            $result = $user->update($validated);

            if($result){
                return response()->json('Обновлено', 200);  
            }else{
                return response()->json('Невозможно обновить', 406);
            }

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
      
    }


    public function getUser(GetUserRequest $request, User $user)
    {
        try {
            return response()->json($user, 200);
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }
}