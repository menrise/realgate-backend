<?php
namespace App\Http\Controllers\Meet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateMeetRequest;
use App\Http\Requests\GetMeetsRequest;
use App\Models\Meet;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;


class MeetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(GetMeetsRequest $request, Meet $meet)
    {
        $validated = $request->validated();


        if( !empty($validated['google_place_ids']) ){

            try {

                $google_place_ids = json_decode($validated['google_place_ids']);

                if( count($google_place_ids) < 1 ){
                    return abort(400);
                }

                $meets = $meet->getByGoogleIds($google_place_ids);

                return response()->json($meets, 200);

            } catch (\Exception $e) {
                \Log::error($e);
                return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
            }

        }


        if( !empty($validated['composite_meet_ids']) ){

            try {

                $composite_meet_ids = json_decode($validated['composite_meet_ids']);

                if( count($composite_meet_ids) < 1 ){
                    return abort(400);
                }

                $meets = $meet->getByCompositeIds($composite_meet_ids);

                return response()->json($meets, 200);

            } catch (\Exception $e) {
                \Log::error($e);
                return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
            }

        }


        try {

            $auth_user = Auth::user();
            $meets = $meet->getByUserId($auth_user->id);

            return response()->json($meets, 200);

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function show(Meet $meet)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function edit(Meet $meet)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMeetRequest $request, Meet $meet)
    {   
        $validatedAction = $request->validated();

        if($validatedAction['action'] === 'unorder'){
            return $this->unorder($request, $meet);
        }elseif($validatedAction['action'] === 'order'){
            return $this->order($request, $meet);
        }elseif($validatedAction['action'] === 'approve_membering'){
            return $this->approve_membering($request, $meet);
        }elseif($validatedAction['action'] === 'unapprove_membering'){
            return $this->unapprove_membering($request, $meet);
        }else{
            return response()->json('Bad request', 400); 
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meet $meet)
    {
        //
    }

    /**
     * Unorder meet.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    private function unorder(UpdateMeetRequest $request, Meet $meet){
        $user_groups = Cache::remember('user_groups', config('cache.long_cache_time'), function () {
            return DB::table('user_groups')->get();
        });

        $validated = Validator::make($request->all(), [
            'meet_time' => 'required|String|max:100',
            'google_place_id' => 'required|String|max:500',
            'user_group' => 'required|min:1|max:' . count($user_groups),
        ])->validate();

        try {

            $meet = $meet->firstOrCreate([
                'google_place_id' => $validated['google_place_id'],
                'meet_time' => $validated['meet_time'],
                'user_group' => $validated['user_group'],   
                'composite_meet_id' => $validated['meet_time'].$validated['user_group'].$validated['google_place_id']   
            ]);   
            $auth_user = Auth::user();

            $meet->users()->detach($auth_user->id);
            return response()->json('Участие отменено', 200); 

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }


    /**
     * Order meet.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    private function order(UpdateMeetRequest $request, Meet $meet){

        $user_groups = Cache::remember('user_groups', config('cache.long_cache_time'), function () {
            return DB::table('user_groups')->get()->all();
        });
        
        $validated = Validator::make($request->all(), [
            'meet_time' => 'required|String|max:100',
            'google_place_id' => 'required|String|max:500',
            'user_group' => 'required|min:1|max:' . count($user_groups),
            'google_place_id_for_query' => 'required|String|max:500',
            'place_latitude' => 'numeric|max:300|min:-300',
            'place_longitude' => 'numeric|max:300|min:-300',
        ])->validate();

        try {

            $meet = $meet->firstOrCreate([
                'google_place_id' => $validated['google_place_id'],
                'meet_time' => $validated['meet_time'],
                'user_group' => $validated['user_group'],   
                'composite_meet_id' => $validated['meet_time'].$validated['user_group'].$validated['google_place_id'],
                'google_place_id_for_query' => $validated['google_place_id_for_query']
            ]);
  
            $need_update_place_latitude = false;                    
            if( !$meet->place_latitude AND !empty($validated['place_latitude']) ){
                $meet->place_latitude = $validated['place_latitude'];
                $need_update_place_latitude = true;
            }

            $need_update_place_longitude = false;
            if( !$meet->place_longitude AND !empty($validated['place_longitude']) ){
                $meet->place_longitude = $validated['place_longitude'];
                $need_update_place_longitude = true;
            }

            if( 
                   ($need_update_place_latitude AND !empty($validated['place_latitude']))
                OR ($need_update_place_longitude AND !empty($validated['place_longitude'])) 
            ){
                $meet->save();
            }

            $user_group = (int)$validated['user_group'];
            if(gettype($user_groups) === 'object'){
                $user_groups = $user_groups->toArray();
            }
            $user_group_data = $user_groups[ array_search($user_group, array_column($user_groups, 'id')) ];
            $auth_user = Auth::user();

            if( !in_array((int)$auth_user->sex, [1, 2], true) ){
                return response()->json('Вы не выбрали свой пол', 406);
            }

            if( !in_array((int)$auth_user->sex_need, [1, 2], true) ){
                return response()->json('Вы не выбрали желаемый пол для знакомств', 406);
            }

            //check meet place for join
            if($meet->users->count() < $user_group_data->max_mans + $user_group_data->max_womans){

                //calculate mans and womans in meeting
                $man = 0;
                $woman = 0;
                foreach ($meet->users as $meet_member) {
                    if($meet_member->pivot->sex === 1){
                        $man++;
                    }else if($meet_member->pivot->sex === 2){
                        $woman++;
                    }else{
                        \Log::error('MeetController.php $meet_member->pivot->sex !== 1 OR 2');
                        return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
                    }
                }


                if($user_group_data->sex_composition_id === 1){

                    if($auth_user->sex === 1 AND $man < $user_group_data->max_mans){
                        $meet->users()->attach($auth_user->id, ['sex' => $auth_user->sex]);
                        return response()->json('Место оформлено', 200); 
                    }else if($auth_user->sex === 2 AND $woman < $user_group_data->max_womans){
                        $meet->users()->attach($auth_user->id, ['sex' => $auth_user->sex]);
                        return response()->json('Место оформлено', 200); 
                    }else{
                        return response()->json('Больше нет мест', 406);
                    } 

                }else if($user_group_data->sex_composition_id === 2){

                    if($auth_user->sex === 1 AND $man < $user_group_data->max_mans){
                        $meet->users()->attach($auth_user->id, ['sex' => $auth_user->sex]);
                        return response()->json('Место оформлено', 200); 
                    }else{
                        return response()->json('Больше нет мест', 406); 
                    }

                }else if($user_group_data->sex_composition_id === 3){

                    if($auth_user->sex === 2 AND $woman < $user_group_data->max_womans){
                        $meet->users()->attach($auth_user->id, ['sex' => $auth_user->sex]);
                        return response()->json('Место оформлено', 200); 
                    }else{
                        return response()->json('Больше нет мест', 406); 
                    }

                }
                
            }else{
                return response()->json('Больше нет мест', 406); 
            }

            return response()->json('Bad request', 400); 

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }


    /**
     * Approve membering of meet.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    private function approve_membering(UpdateMeetRequest $request, Meet $meet){

        $validated = Validator::make($request->all(), [
            'meet_id' => 'required|Integer',
        ])->validate();

        try {

            $meet = $meet->where('id', $validated['meet_id'])->first();
            $auth_user = Auth::user();
            
            $meet->users()->updateExistingPivot(
                $auth_user->id,
                ['approve_membering' => true, 'approve_membering_at' => \Carbon\Carbon::now()]
            );
            broadcast( new \App\Events\MeetUpdate($meet) );

            return response()->json('Участие подтверждено', 200); 

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }


    /**
     * Unapprove membering of meet.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    private function unapprove_membering(UpdateMeetRequest $request, Meet $meet){
        $validated = Validator::make($request->all(), [
            'meet_id' => 'required|Integer',
        ])->validate();

        try {

            $meet = $meet->where('id', $validated['meet_id'])->first();
            $auth_user = Auth::user();
            $meet->users()->updateExistingPivot(
                $auth_user->id,
                ['approve_membering' => false, 'approve_membering_at' => \Carbon\Carbon::now()]
            );
            broadcast( new \App\Events\MeetUpdate($meet) );

            return response()->json('Участие отменено', 200); 

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }
    }
}
