<?php
namespace App\Http\Controllers\Meet;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UpdateLikeRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Validator;


class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLikeRequest $request, User $user)
    {   

        try {

            $validatedAction = $request->validated();
            $liked_user_id = $validatedAction['liked_user_id'];
            $meet_id = $validatedAction['meet_id'];
            $liked_user_ids_arr = explode(',', $liked_user_id);
            $auth_user = Auth::user();

            foreach($liked_user_ids_arr as $key => $user_id){
                $liked_user_ids_for_atach[$user_id] = ['meet_id' => $meet_id];
            }

            $auth_user->iLiked()->attach($liked_user_ids_for_atach);
            return response()->json('Участие отменено', 200);

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json('Внутренняя ошибка сервера. Повторите позже', 500); 
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meet  $meet
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        //
    }
}
