<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'sex', 'sex_need', 'birth_date', 'user_group', 'phone_number', 'device_time', 'device_time_at_server_time', 'avatar', 'latitude', 'longitude'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ["serverTime"];

    public function getServerTimeAttribute() {
         return \Carbon\Carbon::now();
    }

    /**
     * User own profile images.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function profileImages()
    {
        return $this->hasMany('App\Models\ProfileImage', 'user_id', 'id');
    }        

    /**
     * User may have manciple social accounts.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function socialAccounts()
    {
        return $this->hasMany(SocialAccount::class);
    }

    /**
     * User own adventures.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adventuresOwner()
    {
        return $this->hasMany('App\Models\Adventure\Adventure', 'owner_id', 'id')->fresh();
    }    


    /**
     * Fresh user own adventures.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function adventuresOwnerFresh()
    {
        return $this->adventuresOwner()->fresh();
    }

    /**
     * Adventures that user member of.
     */
    public function adventuresMember()
    {
        return $this->belongsToMany('App\Models\Adventure\Adventure')->withTimestamps()
            ->withPivot(['approve_membering', 'approve_membering_at', 'sex']);
    }

    /**
     * Fresh adventures that user member of.
     */
    public function adventuresMemberFresh()
    {
        return $this->adventuresMember()->fresh();
    }

    /**
     * The meets that belong to the user.
     */
    public function meets()
    {
        return $this->belongsToMany('App\Models\Meet')->withTimestamps()
            ->withPivot(['approve_membering', 'approve_membering_at', 'sex']);
    }

    /**
     * The liked users (who I liked).
     */
    public function iLiked()
    {
        return $this->belongsToMany('App\Models\User', 'likes', 'liker_user_id', 'liked_user_id')->withTimestamps()->withPivot(['meet_id']);
    }

    /**
     * The users who liked me. 
     */
    public function likedMe()
    {
        return $this->belongsToMany('App\Models\User', 'likes', 'liked_user_id', 'liker_user_id')->withTimestamps()->withPivot(['meet_id']);
    }

    /**
     * The canceled meets of this user. //right now only automatic cancel by task SendNotEnoughMeetMembersPush.php
     */
    public function сanceledMeetMemberings()
    {
        return $this->belongsToMany('App\Models\Meet', 'canceled_meet_memberings', 'user_id', 'meet_id')
            ->withTimestamps()->withPivot(['not_enough_members']);
    }    

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}