<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Meet extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'google_place_id', 'meet_time', 'user_group', 'composite_meet_id', 'google_place_id_for_query'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    protected $appends = ["serverTime"];

    public function getServerTimeAttribute() {
         return \Carbon\Carbon::now();
    }

    /**
     * The users that belong to the meet.
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User')->withTimestamps()->withPivot(['approve_membering', 'approve_membering_at', 'sex']);
    }

    /**
     * The users that belong to the meet.
     */
    public function likes()
    {
        return $this->belongsToMany('App\Models\User', 'likes', 'meet_id', 'liked_user_id')
            ->withTimestamps()->withPivot(['liker_user_id']);
    }

    /**
     * The meets that belong to the user.
     */
    public function userGroups()
    {
        return $this->belongsTo('App\Models\UserGroups', 'user_group', 'id');
    }

    /**
     * The users who canceled this meet. //right now only automatic cancel by task SendNotEnoughMeetMembersPush.php
     */
    public function usersCanceledMembering()
    {
        return $this->belongsToMany('App\Models\User', 'canceled_meet_memberings', 'meet_id', 'user_id')
            ->withTimestamps()->withPivot(['not_enough_members']);
    }    

    public function getByGoogleIds($google_place_ids_arr)
    {
        $meets = $this->whereIn('google_place_id', $google_place_ids_arr)
            ->with('users')
            ->with('likes')
            ->with('userGroups')
            ->has('users')
            ->withCount([
            'users as man_count' => function ($query) {
                $query->where('meet_user.sex', 1);
            },
            'users as woman_count' => function ($query) {
                $query->where('meet_user.sex', 2);
            }
        ])->get();
        
        return $meets;
    }

    public function getByCompositeIds($composite_meet_ids_arr)
    {
        $meets = $this->whereIn('composite_meet_id', $composite_meet_ids_arr)
            ->with('users')
            ->with('likes')
            ->with('userGroups')
            ->has('users')
            ->withCount([
            'users as man_count' => function ($query) {
                $query->where('meet_user.sex', 1);
            },
            'users as woman_count' => function ($query) {
                $query->where('meet_user.sex', 2);
            }
        ])->get();
        
        return $meets;
    }

    public function getByUserId($user_id)
    {   

        $startTime = Carbon::now()->subDays(30)->toDateTimeString();

        $meets = $this->where('meet_time', '>', $startTime)
            ->whereHas('users', function ($query) use ($user_id) {
                $query->where('user_id', '=', $user_id);
            })
            ->with('users')
            ->with('likes')
            ->with('userGroups')
            ->withCount([
                'users as man_count' => function ($query) {
                    $query->where('meet_user.sex', 1);
                },
                'users as woman_count' => function ($query) {
                    $query->where('meet_user.sex', 2);
                }
            ])
            ->get();
        
        return $meets;
    }
}
