<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class PrivateMessage extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id', 'receiver_id', 'message'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    

    public function sender()
    {
        return $this->belongsTo('App\Models\User', 'sender_id', 'id');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Models\User', 'receiver_id', 'id');
    }

    public function setMessageReaded($message_id){
        if(!$message_id) {return false;}

        $res = $this->where('id', $message_id)
            ->update(['readed' => 1]);

        return $res;
    }

    public function getMessages($first_user, $second_user, $messages_limit = 50){
        $messages = $this->where('sender_id', '=', $first_user->id)
            ->where('receiver_id', '=', $second_user->id)
            ->orWhere(function (Builder $query) use ($first_user, $second_user) {
                $query->where('sender_id', '=', $second_user->id)
                    ->where('receiver_id', '=', $first_user->id);
            })
            ->with('sender')
            ->with('receiver')
            ->orderBy('id', 'DESC')
            ->take($messages_limit)
            ->get();

        return $messages;
    }

}