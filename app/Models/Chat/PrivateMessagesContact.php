<?php

namespace App\Models\Chat;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class PrivateMessagesContact extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_1_id', 'user_2_id', 
        'user_1_received_messages', 'user_2_received_messages',
        'user_1_received_messages_before_read', 'user_2_received_messages_before_read',
        'user_1_last_readed_mes_id', 'user_2_last_readed_mes_id',
        'last_mes_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];
    

    public function user1()
    {
        return $this->belongsTo('App\Models\User', 'user_1_id', 'id');
    }

    public function user2()
    {
        return $this->belongsTo('App\Models\User', 'user_2_id', 'id');
    }

    public function user1LastReadedMessage()
    {
        return $this->belongsTo('App\Models\Chat\PrivateMessage', 'user_1_last_readed_message_id', 'id');
    }

    public function user2LastReadedMessage()
    {
        return $this->belongsTo('App\Models\Chat\PrivateMessage', 'user_2_last_readed_message_id', 'id');
    }


    public function createOrUpdateContact($sender_id, $receiver_id, $message_id){

        if($sender_id <= $receiver_id){
            $user_1_id = $sender_id;
            $user_2_id = $receiver_id;
        }else{
            $user_1_id = $receiver_id;
            $user_2_id = $sender_id;               
        }

        $privateMessagesContact = $this->firstOrNew(
            ['user_1_id' => $user_1_id, 'user_2_id' => $user_2_id] 
        );

        if($privateMessagesContact->exists){
            if($user_1_id === $receiver_id){
                $privateMessagesContact->increment('user_1_received_messages');
            }else{
                $privateMessagesContact->increment('user_2_received_messages');
            }
        }else{
            if($user_1_id === $receiver_id){
                $privateMessagesContact->user_1_received_messages = 1;
            }else{
                $privateMessagesContact->user_2_received_messages = 1;
            }                
        }

        $privateMessagesContact->last_mes_id = $message_id;

        $privateMessagesContact->save();
    }

    public function setMessagesReaded($sender_id, $receiver_id){

        if($sender_id <= $receiver_id){
            $user_1_id = $sender_id;
            $user_2_id = $receiver_id;
        }else{
            $user_1_id = $receiver_id;
            $user_2_id = $sender_id;               
        }

        $privateMessagesContact = $this->where('user_1_id', $user_1_id)
            ->where('user_2_id', $user_2_id)
            ->first();

        if($privateMessagesContact){
            if($user_1_id === $receiver_id){
                $privateMessagesContact->user_1_received_messages_before_read = $privateMessagesContact->user_1_received_messages;
            }else{
                $privateMessagesContact->user_2_received_messages_before_read = $privateMessagesContact->user_2_received_messages;
            }   
            
            $privateMessagesContact->save();
        }
    }


    public function getContacts($user, $contacts_limit = 50){

        $contacts = $this->where('user_1_id', '=', $user->id)
            ->orWhere('user_2_id', '=', $user->id)
            ->with('user1')
            ->with('user2')
            ->orderBy('last_mes_id', 'DESC')
            ->take($contacts_limit)
            ->get();

        return $contacts;
    }     
    
}