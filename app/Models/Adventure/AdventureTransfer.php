<?php
namespace App\Models\Adventure;
use Illuminate\Database\Eloquent\Model;
class AdventureTransfer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

}