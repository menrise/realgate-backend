<?php

namespace App\Models\Adventure;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Adventure extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner_id', 'adventure_type_id', 'adventure_spend_id',
        'adventure_transfer_id', 'adventure_relation_id',
        'name', 'description',  'time',  'time_by_agreement',
        'latitude', 'longitude', 'need_mans_count', 'need_womans_count',
        'age_start', 'age_finish'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * Scope a query to only include near to coordinates adventures.
     *
     */
    public function scopeNear($query, $latitude, $longitude)
    {

        $latitude_diapason = config('app.latitude_diapason_within_user_get_near_adventures');
        $longitude_diapason = config('app.longitude_diapason_within_user_get_near_adventures');

        return $query
            ->where('latitude', '<', $latitude + $latitude_diapason)
            ->where('latitude', '>', $latitude - $latitude_diapason)
            ->where('longitude', '<', $longitude + $longitude_diapason)
            ->where('longitude', '>', $longitude - $longitude_diapason);
    }

    /**
     * Scope a query to only include fresh adventures.
     *
     */
    public function scopeFresh($query)
    {

        $startTime = \Carbon\Carbon::now()->subDays(30)->toDateTimeString();

        return $query
            ->where('time_by_agreement', '=', 1)
            ->where('adventures.created_at', '>', $startTime)
            ->orWhere(function ($query) use ($startTime) {
                $query->where('time_by_agreement', '=', 0)
                    ->where('time', '>', $startTime);
            });
    }

    /**
     * The users that belong to the meet.
     */
    public function members()
    {
        return $this->belongsToMany('App\Models\User')->withTimestamps()
            ->withPivot(['approve_membering', 'approve_membering_at', 'sex']);
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }

    public function adventureType()
    {
        return $this->belongsTo('App\Models\Adventure\AdventureType', 'adventure_type_id', 'id');
    }

    public function spendType()
    {
        return $this->belongsTo('App\Models\Adventure\AdventureSpend', 'spend_type_id', 'id');
    }

    public function transferType()
    {
        return $this->belongsTo('App\Models\Adventure\TransferType', 'transfer_type_id', 'id');
    }

    public function relationType()
    {
        return $this->belongsTo('App\Models\Adventure\RelationType', 'relation_type_id', 'id');
    }    
    

}