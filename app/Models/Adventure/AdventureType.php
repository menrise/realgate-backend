<?php
namespace App\Models\Adventure;
use Illuminate\Database\Eloquent\Model;
class AdventureType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

}