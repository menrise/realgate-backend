<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ProfileImage extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'is_main', 'file_name'
    ];

    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }
}