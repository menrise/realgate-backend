<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Models\Adventure\Adventure;
use App\Models\User;
use App\Models\Chat\PrivateMessage;
use App\Models\ProfileImage;
use App\Policies\AdventurePolicy;
use App\Policies\UserPolicy;
use App\Policies\PrivateMessagePolicy;
use App\Policies\ProfileImagePolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Adventure::class => AdventurePolicy::class,
        User::class => UserPolicy::class,
        PrivateMessage::class => PrivateMessagePolicy::class,
        ProfileImage::class => ProfileImagePolicy::class,        
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
