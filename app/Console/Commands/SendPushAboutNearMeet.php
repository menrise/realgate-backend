<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Meet;
use Carbon\Carbon;
use App\Notifications\NearMeetNotice;
use Illuminate\Database\Eloquent\Builder;


class SendPushAboutNearMeet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'push-about-near-meet:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push about near meet to needed users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $meets = Meet::whereDate('meet_time', '>=', Carbon::now()->addHours(3) )
            ->with('users')
            ->with('userGroups')
            ->get();

        foreach ($meets as $key => $meet) {
            $query = $this->getSuitedUsersQuery($meet);
            $users = $query->get();
            $query->update(['notice_near_meet_at' => Carbon::now()]);
            $this->sendPush($users, $meet);
        }        
        
    }

    /**
     * Execute the console command.
     * @param $users - eloquent collection
     *
     * @return array of count mans and womans
     */
    protected function calcUsersSex($users){
        $mans = 0;
        $womans = 0;
        foreach ($users as $key => $user) {
            if($user->pivot->sex === 1){
                $mans++;
            }else if($user->pivot->sex === 2){
                $womans++;
            }    
        } 

        return ['mans' => $mans, 'womans' => $womans];        
    }

    /**
     * Execute the console command.
     * @param $meet - eloquent model instance
     *
     * @return eloquent query
     */
    protected function getSuitedUsersQuery($meet){

        try {
            $usersSex = $this->calcUsersSex($meet->users);

            if($meet->userGroups->sex_composition_id === 1){
                //polysex

                if( 
                    $usersSex['mans'] === $meet->userGroups->max_mans  
                AND $usersSex['womans'] === $meet->userGroups->max_womans
                ){
                    return false;
                }

                if($usersSex['mans'] < $usersSex['womans']){
                    //invite mans
                    return $this->getSuitedUsersBasicQuery($meet)
                        ->where('sex', '=', 1)
                        ->where('sex_need', '=', 2); 

                }else if($usersSex['mans'] > $usersSex['womans']){
                    //invite womans
                    return $this->getSuitedUsersBasicQuery($meet)
                        ->where('sex', '=', 2)
                        ->where('sex_need', '=', 1); 

                }else if($usersSex['mans'] === $usersSex['womans']){

                    //invite mans and womans
                    return $this->getSuitedUsersBasicQuery($meet)
                        ->where(function ($query) {
                            $query->where('sex', '=', 2)
                            ->where('sex_need', '=', 1)

                            ->orWhere(function ($query1) {
                                $query1->where('sex', '=', 1)
                                ->where('sex_need', '=', 2);
                            });
                        }); 
                }

            }else if($meet->userGroups->sex_composition_id === 2){
                //monosex mans

                if($usersSex['mans'] === $meet->userGroups->max_mans){
                    return false;
                }

                return $this->getSuitedUsersBasicQuery($meet)
                    ->where('sex', '=', 1)
                    ->where('sex_need', '=', 1); 

            }else if($meet->userGroups->sex_composition_id === 3){
                //monosex womans

                if($usersSex['womans'] === $meet->userGroups->max_womans){
                    return false;
                }    

                return $this->getSuitedUsersBasicQuery($meet)
                    ->where('sex', '=', 2)
                    ->where('sex_need', '=', 2);
            }

        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        } 
    }


    protected function sendPush($pushForUsers, $meet){

        try {

            if( count($pushForUsers) ){
                \Notification::send($pushForUsers, new NearMeetNotice($meet) );
            }
            
        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        }  
    }


    protected function getSuitedUsersBasicQuery($meet){
        try {        
            $basicQuery = User::
              whereDate('birth_date', '>', Carbon::now()->subYears($meet->userGroups->max_age + 1))
            ->whereDate('birth_date', '<', Carbon::now()->subYears($meet->userGroups->min_age))

            ->where('latitude', '<', $meet->place_latitude + 
                config('app.latitude_diapason_within_user_get_near_meet_notice'))
            ->where('latitude', '>', $meet->place_latitude - 
                config('app.latitude_diapason_within_user_get_near_meet_notice'))    

            ->where('longitude', '<', $meet->place_longitude + 
                config('app.longitude_diapason_within_user_get_near_meet_notice'))
            ->where('longitude', '>', $meet->place_longitude - 
                config('app.longitude_diapason_within_user_get_near_meet_notice'))          


            ->where(function ($query) {
                $query->whereDate('notice_near_meet_at', '<', 
                    Carbon::now()->subDays(config('app.days_before_new_notify_near_meet'))
                )->orWhereNull('notice_near_meet_at');
            })
                            
            ->where('get_notice_near_meet', '=', 1)
            ->whereDoesntHave('meets', function (Builder $query) {
                //where does not have future meets
                $query->where('meet_time', '>', Carbon::now());
            })

            ->take(config('app.people_get_neer_meet_notifies_at_once'));

            return $basicQuery;

        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        }  
    }
}