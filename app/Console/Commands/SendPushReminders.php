<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Meet;
use Carbon\Carbon;
use App\Notifications\MeetReminder;


class SendPushReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meet-reminder:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send push reminders of meet to users devices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $pushMinutesBeforeMeetArr = [24*60, 6*60, 3*60, 1*60, 15]; 
        
        $meets = Meet::whereDate('meet_time', '>=', Carbon::now()->subDays(1) )
            ->whereDate('meet_time', '<=', Carbon::now()->addDays(1) )
            ->with('users')
            ->get();

        foreach ($pushMinutesBeforeMeetArr as $key => $pushMinutesBeforeMeet) {
            $this->sendPush($meets, $pushMinutesBeforeMeet);
        }        
        
    }

    protected function sendPush($meets, $pushMinutesBeforeMeet){

        $gapMinutes = 11; //interval of cron + 1 min for one call in gap

        try {

            $pushForUsers = [];

            foreach ($meets as $meetsKey => $meet) {
                
                if( count($meet->users) > 0 ){
                    
                    foreach ($meet->users as $usersKey => $user) {

                        if($user->device_time AND $user->device_time_at_server_time){

                            $device_time = Carbon::parse($user->device_time);
                            $device_time_at_server_time = Carbon::parse($user->device_time_at_server_time);

                            $diffSeconds = $device_time->getTimestamp() - $device_time_at_server_time->getTimestamp();
                            $onUserDeviceTimeNow = Carbon::now()
                                ->add( \DateInterval::createFromDateString($diffSeconds.' seconds') );

                            //need to Carbon::parse($meet->meet_time) each time because sub() method mutate the owner
                            $startTimeForRemind = Carbon::parse($meet->meet_time)
                                ->sub( \DateInterval::createFromDateString($pushMinutesBeforeMeet.' minutes') )
                                ->sub( \DateInterval::createFromDateString($gapMinutes.' minutes') );
                            $endTimeForRemind = Carbon::parse($meet->meet_time)
                                ->sub( \DateInterval::createFromDateString($pushMinutesBeforeMeet.' minutes') );


                            $isTimeToRemind = $onUserDeviceTimeNow
                                ->between($startTimeForRemind, $endTimeForRemind, false);
                            
                            // \Log::info(PHP_EOL);
                            // \Log::info(PHP_EOL);
                            // \Log::info('$isTimeToRemind: ');
                            // \Log::info( json_encode($isTimeToRemind) );


                            // \Log::info($onUserDeviceTimeNow);
                            // \Log::info($startTimeForRemind);
                            // \Log::info($endTimeForRemind);

                            // \Log::info(PHP_EOL);
                            // \Log::info(PHP_EOL);                            

                            if($isTimeToRemind){
                                $pushForUsers[] = $user;
                            }
                        }
                    }
                }
                
            }

            if( count($pushForUsers) ){
                \Notification::send($pushForUsers, new MeetReminder($pushMinutesBeforeMeet) );
            }

        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        }  
    }
}
