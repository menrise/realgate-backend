<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Meet;
use Carbon\Carbon;
use App\Notifications\MeetMemberingCancel;


class SendNotEnoughMeetMembersPush extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'not-enough-meet-members-push:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send not enough meet members push and cancel membering of user that have not any pair in meet';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $pushMinutesBeforeMeetArr = [1*50]; 
        
        $meets = Meet::whereDate('meet_time', '>=', Carbon::now()->subDays(10) )
            ->whereDate('meet_time', '<=', Carbon::now()->addDays(10) )
            ->with('users')
            ->with('userGroups')
            ->get();

        foreach ($pushMinutesBeforeMeetArr as $key => $pushMinutesBeforeMeet) {
            $this->sendPush($meets, $pushMinutesBeforeMeet);
        }        
        
    }

    protected function sendPush($meets, $pushMinutesBeforeMeet){

        $gapMinutes = 11; //interval of cron + 1 min for one call in gap

        try {

            $pushForUsers = [];

            foreach ($meets as $meetsKey => $meet) {
                
                if( count($meet->users) > 0 ){

                    $mans = 0;
                    $womans = 0;
                    foreach ($meet->users as $usersKey => $user) {
                        if($user->pivot->sex === 1){
                            $mans++;
                        }else if($user->pivot->sex === 2){
                            $womans++;
                        }else{
                            \Log::error('SendNotEnoughMeetMembersPush.php $user->pivot->sex !== 1 OR 2');
                        }
                    }
                    
                    foreach ($meet->users as $usersKey => $user) {

                        if($user->device_time AND $user->device_time_at_server_time){

                            $device_time = Carbon::parse($user->device_time);
                            $device_time_at_server_time = Carbon::parse($user->device_time_at_server_time);

                            $diffSeconds = $device_time->getTimestamp() - $device_time_at_server_time->getTimestamp();
                            $onUserDeviceTimeNow = Carbon::now()
                                ->add( \DateInterval::createFromDateString($diffSeconds.' seconds') );

                            //need to Carbon::parse($meet->meet_time) each time because sub() method mutate the owner
                            $startTimeForRemind = Carbon::parse($meet->meet_time)
                                ->sub( \DateInterval::createFromDateString($pushMinutesBeforeMeet.' minutes') )
                                ->sub( \DateInterval::createFromDateString($gapMinutes.' minutes') );
                            $endTimeForRemind = Carbon::parse($meet->meet_time)
                                ->sub( \DateInterval::createFromDateString($pushMinutesBeforeMeet.' minutes') );


                            $isTimeToRemind = $onUserDeviceTimeNow
                                ->between($startTimeForRemind, $endTimeForRemind, false);
                            
                            if($isTimeToRemind){ 
                                //cancel membering           
                                if( $meet->userGroups->sex_composition_id === 1 AND ($mans === 0 OR $womans === 0) ){ //multisex
                                    $user->meets()->detach([$meet->id]);
                                    $user->сanceledMeetMemberings()->attach($meet->id, ['not_enough_members' => true]);
                                    $pushForUsers[] = $user;
                                    \Log::info("Cancel membering of meet_id: $meet->id, user_id: $user->id");
                                }elseif($meet->userGroups->sex_composition_id === 2 AND $mans < 2){ //monosex
                                    $user->meets()->detach([$meet->id]);
                                    $user->сanceledMeetMemberings()->attach($meet->id, ['not_enough_members' => true]);
                                    $pushForUsers[] = $user;  
                                    \Log::info("Cancel membering of meet_id: $meet->id, user_id: $user->id");
                                }elseif($meet->userGroups->sex_composition_id === 3 AND $womans < 2){ //monosex
                                    $user->meets()->detach([$meet->id]);
                                    $user->сanceledMeetMemberings()->attach($meet->id, ['not_enough_members' => true]);
                                    $pushForUsers[] = $user;      
                                    \Log::info("Cancel membering of meet_id: $meet->id, user_id: $user->id");
                                }
                            }

                        }
                    }
                }
                
            }

            if( count($pushForUsers) ){
                \Notification::send($pushForUsers, new MeetMemberingCancel($pushMinutesBeforeMeet) );
            }

        } catch (\Exception $e) {
            \Log::error($e);
            $this->error($e);
        }  
    }
}
