<?php

namespace App\Policies;

use App\Models\User;
use App\Models\ProfileImage;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProfileImagePolicy
{
    use HandlesAuthorization;

    public function update(User $user, ProfileImage $profileImage)
    {
        return $user->id === $profileImage->user_id;
    }

    public function delete(User $user, ProfileImage $profileImage)
    {
        return $user->id === $profileImage->user_id;
    }
}
