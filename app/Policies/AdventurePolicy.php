<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Adventure\Adventure;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdventurePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the adventure.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Adventure\Adventure  $adventure
     * @return mixed
     */
    public function view(User $user, Adventure $adventure)
    {
        //
    }

    /**
     * Determine whether the user can create adventures.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the adventure.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Adventure\Adventure  $adventure
     * @return mixed
     */
    public function update(User $user, Adventure $adventure)
    {
        return $user->id === $adventure->owner_id;
    }

    /**
     * Determine whether the user can delete the adventure.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Adventure\Adventure  $adventure
     * @return mixed
     */
    public function delete(User $user, Adventure $adventure)
    {
        return $user->id === $adventure->owner_id;
    }

    /**
     * Determine whether the user can restore the adventure.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Adventure\Adventure  $adventure
     * @return mixed
     */
    public function restore(User $user, Adventure $adventure)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the adventure.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Adventure\Adventure  $adventure
     * @return mixed
     */
    public function forceDelete(User $user, Adventure $adventure)
    {
        //
    }
}
