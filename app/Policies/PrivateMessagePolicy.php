<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Chat\PrivateMessage;
use Illuminate\Auth\Access\HandlesAuthorization;

class PrivateMessagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can read the private message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\PrivateMessage  $privateMessage
     * @return mixed
     */
    public function read(User $user, PrivateMessage $privateMessage)
    {   
        if($privateMessage->receiver_id === $user->id){
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create private messages.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the private message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\PrivateMessage  $privateMessage
     * @return mixed
     */
    public function update(User $user, PrivateMessage $privateMessage)
    {
        //
    }

    /**
     * Determine whether the user can delete the private message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\PrivateMessage  $privateMessage
     * @return mixed
     */
    public function delete(User $user, PrivateMessage $privateMessage)
    {
        //
    }

    /**
     * Determine whether the user can restore the private message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\PrivateMessage  $privateMessage
     * @return mixed
     */
    public function restore(User $user, PrivateMessage $privateMessage)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the private message.
     *
     * @param  \App\Models\User  $user
     * @param  \App\PrivateMessage  $privateMessage
     * @return mixed
     */
    public function forceDelete(User $user, PrivateMessage $privateMessage)
    {
        //
    }
}
