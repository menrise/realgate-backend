<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function send_private_message(User $auth_user, User $receiver_user)
    {
        //TODO BAN LOGIC
        return true;
    }
}
