<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class AdventureApproveNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($adventure, $usersArrValIsAprroveIndexIsId)
    {
        $this->adventure = $adventure;
        $this->usersArrValIsAprroveIndexIsId = $usersArrValIsAprroveIndexIsId;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }



    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }

    public function toExpoPush($notifiable)
    {   

        if (empty($this->adventure)) {
            \Log::error('AdventureApproveNotification.php empty($this->adventure)');
            return;
        }

        if($this->usersArrValIsAprroveIndexIsId[$notifiable->id]){
            $title = "Вас приняли в приключение - ".$this->adventure->name;
            $body = "Детали приключения вы можете уточнить у его создателя."; 
        }else{
            $title = "Вас не приняли в приключение - ".$this->adventure->name;
            $body = "Не огорчайтесь, у нас много приключений, выберите любое другое."; 
        }    

        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->setChannelId('reminders')
            ->title($title)
            ->body($body)
            ->setJsonData(['name' => $title, 'description' => $body]);
    }
}
