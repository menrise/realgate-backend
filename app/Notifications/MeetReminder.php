<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class MeetReminder extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($remindMinutesBeforeMeet)
    {
        $this->remindMinutesBeforeMeet = $remindMinutesBeforeMeet;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }

    public function toExpoPush($notifiable)
    {   

        if ($this->remindMinutesBeforeMeet < 1) {
            return;
        }

        $hours = floor($this->remindMinutesBeforeMeet / 60);
        $minutes = ($this->remindMinutesBeforeMeet % 60);
        $format = '%02d:%02d';
        $time = sprintf($format, $hours, $minutes);

        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->setChannelId('reminders')
            ->title("Напоминание о встрече")
            ->body("У вас назначена встреча в ТРЦ через $time.")
            ->setJsonData(['name' => 'Напоминание о встрече', 'description' => "У вас назначена встреча в ТРЦ через $time."]);
    }
}
