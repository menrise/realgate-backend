<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class MeetMemberingCancel extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($remindMinutesBeforeMeet)
    {
        $this->remindMinutesBeforeMeet = $remindMinutesBeforeMeet;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }

    public function toExpoPush($notifiable)
    {   

        if ($this->remindMinutesBeforeMeet < 1) {
            return;
        }

        $title = "Ваше участие во встрече отменено";
        $body = "Из-за того что на встречу зарегистрировалось недостаточно участников мы отменили ваше участие. Если вы находитесь поблизости от места встречи и готовы прийти в случае появления новых учасников, зарегистрируйтесь во встрече заного. Встреча всё ещё может состояться, но вероятность мала.";

        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->setChannelId('reminders')
            ->title($title)
            ->body($body)
            ->setJsonData(['name' => $title, 'description' => $body]);
    }
}
