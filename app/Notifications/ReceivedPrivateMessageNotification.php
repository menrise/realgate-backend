<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class ReceivedPrivateMessageNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($privateMessage, $receiver)
    {
        $this->privateMessage = $privateMessage;
        $this->receiver = $receiver;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }



    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }

    public function toExpoPush($notifiable)
    {   

        if (empty($this->privateMessage)) {
            \Log::error('ReceivedPrivateMessageNotification.php empty($this->privateMessage)');
            return;
        }

        $title = "Вам сообщение от ".$this->receiver->name;
        $body = "Вам поступило приватное сообщение";        

        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->setChannelId('reminders')
            ->title($title)
            ->body($body)
            ->setJsonData(['name' => $title, 'description' => $body, 'user_id' => $this->privateMessage->sender_id, 'type' => 'received_private_message']);
    }
}
