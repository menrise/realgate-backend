<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class AdventureOwnerGetCandidateNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($candidate)
    {
        $this->candidate = $candidate;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }



    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }

    public function toExpoPush($notifiable)
    {   

        if (empty($this->candidate)) {
            \Log::error('AdventureOwnerGetCandidateNotification.php empty($this->candidate)');
            return;
        }

        $title = $this->candidate->name." хочет в ваше приключение";
        $body = "В ваше приключение добавился новый кандидат. Примите или отклоните его.";        

        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->setChannelId('reminders')
            ->title($title)
            ->body($body)
            ->setJsonData(['name' => $title, 'description' => $body]);
    }
}
