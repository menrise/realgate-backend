<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\ExpoPushNotifications\ExpoChannel;
use NotificationChannels\ExpoPushNotifications\ExpoMessage;

class NearMeetNotice extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($meet)
    {
        $this->google_place_id_for_query = $meet->google_place_id_for_query;
        $this->google_place_id = $meet->google_place_id;
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }


    public function via($notifiable)
    {
        return [ExpoChannel::class];
    }

    public function toExpoPush($notifiable)
    {   

        if (empty($this->google_place_id_for_query)) {
            \Log::error('NearMeetNotice.php empty($this->google_place_id_for_query)');
            return;
        }

        $title = "Встреча рядом";
        $body = "Поблизости запланирована встреча на которой не хватает человека похожего на вас. Другие участники будут рады знакомству с вами, участвуйте!";        

        return ExpoMessage::create()
            ->badge(1)
            ->enableSound()
            ->setChannelId('reminders')
            ->title($title)
            ->body($body)
            ->setJsonData(['name' => $title, 'description' => $body, 'google_place_id_for_query' => $this->google_place_id_for_query, 'google_place_id' => $this->google_place_id]);
    }
}
