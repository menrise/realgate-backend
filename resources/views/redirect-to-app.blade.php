<!doctype html>
<html lang="{{ app()->getLocale() }}" style="height: 100%;">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Перенаправление в приложение</title>
    </head>
    <body style="height: 100%;">
        <div style="
            height: 100%;
            display: flex;
            align-items: center;
            justify-content: center;"
        >
            <div style="text-align: center;">
                <a 
                    style="font-size:30px;"
                    href="{{ $deep_link }}"
                >
                    Вернуться в приложение
                </a>
            </div>
        </div>
        <script>window.location.replace("{!! $deep_link !!}");</script>
    </body>
</html>
