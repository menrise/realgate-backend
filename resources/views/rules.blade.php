<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Правила пользования сервисом</title>

        <!-- Styles -->
        <style>
            html, body {
                font-weight: 200;
                color: #636b6f;
                background-color: #fff;
                font-size: 14px;
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: left;
                max-width: 800px;
            }

            .title {
                font-size: 1.2rem;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    <strong>Правила пользования сервисом</strong>
                </div>

                <p> 
                    Пользователь обязуется уважительно отноститься к другим пользователям сервиса. У нас запрещены оскорбления в любом виде.
                </p>

                <p> 
                    Запрещены любые способы и просьбы получения контактов других пользователей на встречах, кроме механизма взаимных симпатий. Контакты передаются только при взаимной симпатии. Передача контактов осуществляется через программное обеспечение сервиса.
                </p>    

                <p> 
                    Не стоит навязывать общение после встречи. Уважайте чувства и время других людей. У каждого участника есть 5 минут во время знакомства для того чтобы заинтересовать симпатичного ему человека. Если ваши симпатии не совпали участвуйте ещё, возможно вы снова встретитесь или найдёте кого-то другого более интересного для вас.
                </p>     
                 
                <p> 
                    За нарушение правил или при жалобах на пользователя администрация оставляет за собой право прекратить ему доступ к сервису.
                </p>                                            
            </div>
        </div>


        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(52836709, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/52836709" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    </body>
</html>
