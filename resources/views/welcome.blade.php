<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>RealGate - быстрые знакомства вживую</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300" rel="stylesheet">
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Montserrat', sans-serif;
                font-weight: 200;
                margin: 0;
                font-size: 14px;
            }
            .menu a{
                text-decoration: none;
                color: #636b6f;
                font-family: 'Montserrat', sans-serif;
                font-weight: 200;
                margin: 0;
                font-size: 14px;
                margin-right: 10px; 

            }
            .menu{
                margin-top:70px; 
            }              
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title > img {
                height: 300px;
                height: 32vh;
                cursor: pointer;
            }

            .title {
                font-size: 1.2rem;
            }

            .links img {
                padding: 0 25px;
                width: 170px;
                cursor: pointer;
            }

            .links a {
                text-decoration: none;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .box-shadow{
                box-shadow: 0 0 10px rgba(0,0,0,0.5);
            }
            .promo-video{
                width: 840px;
                height: 472px;
                width: 80vw;
                height: 50vh;
                max-width: 840px;
                max-height: 472px;
                min-width: 320px;
                background-image: url('/images/promo-video-img.jpg');
                background-repeat: no-repeat;
                background-color: #000;
                background-position: center;
                background-size: 100%;
                margin: auto;
            }
            /** 
             * iPad with portrait orientation.
             */
            @media all and (device-width: 768px) and (device-height: 1024px) and (orientation:portrait){
                .promo-video {
                    height: 512px;
                }
                .title > img {
                    height: 768px;
                }
            }

            /** 
             * iPad with landscape orientation.
             */
            @media all and (device-width: 768px) and (device-height: 1024px) and (orientation:landscape){
                .promo-video {
                    height: 384px;
                }
                .title > img {
                    height: 576px;
                }
            }

            /**
             * iPhone 5
             * You can also target devices with aspect ratio.
             */
            @media screen and (device-aspect-ratio: 40/71) {
                .promo-video {
                    height: 250px;
                }
                .title > img {
                    height: 375px;
                }              
            }

            .promo-video {
                -webkit-animation: promo-maximaze 4s ease-in-out; /* Safari 4.0 - 8.0 */
                animation: promo-maximaze 4s ease-in-out;
            }

            .title > img {
                -webkit-animation: title-img-minimaze 4s ease-in-out; /* Safari 4.0 - 8.0 */
                animation: title-img-minimaze 4s ease-in-out;
            } 

            .title > img:active {
                -webkit-animation: title-img-maximaze 5s ease-in-out; /* Safari 4.0 - 8.0 */
                animation: title-img-maximaze 5s ease-in-out;
            } 

            /* Safari 4.0 - 8.0 */
            @-webkit-keyframes title-img-minimaze {
                0% {
                    height: 80vh;
                }
                25% {
                    height: 80vh;
                }
                100% {
                    height: 32vh;
                }
            }

            /* Standard syntax */
            @keyframes title-img-minimaze {
                0% {
                    height: 80vh;
                }
                25% {
                    height: 80vh;
                }
                100% {
                    height: 32vh;
                }
            }

            /* Safari 4.0 - 8.0 */
            @-webkit-keyframes title-img-maximaze {
                0% {
                    height: 35vh;
                }
                100% {
                    height: 75vh;
                }
            }

            /* Standard syntax */
            @keyframes title-img-maximaze {
                0% {
                    height: 35vh;
                }
                100% {
                    height: 75vh;
                }
            }

            /* Safari 4.0 - 8.0 */
            @-webkit-keyframes promo-maximaze {
                0% {
                    height: 0px;
                    width: 0px;
                }
                25% {
                    height: 0px;
                    width: 0px;
                    transform: scaleY(0);
                }
                60% {
                    box-shadow: 0 0 10px rgba(0,0,0,0.5);
                    border-radius: 90%;
                }
                100% {
                    width: 80vw;
                    height: 50vh;
                }
                75% {
                    box-shadow: 0 0 150px rgba(229,0,0,0.2);
                }
                80% {
                    box-shadow: 0 0 150px rgba(229,0,0,0.14);
                }
                /*
                85% {
                    box-shadow: 0 0 200px rgba(229,0,0,0.2);
                }
                90% {
                    box-shadow: 0 0 200px rgba(229,0,0,0.14);
                }
                */
                85% {
                    box-shadow: 0 0 150px rgba(47,149,220,0.2);
                }
                90% {
                    box-shadow: 0 0 150px rgba(47,149,220,0.14);
                } 
            
                100% {
                    box-shadow: 0 0 10px rgba(0,0,0,0.5);
                }
            }

            /* Standard syntax */
            @keyframes promo-maximaze {
                0% {
                    height: 0px;
                    width: 0px;
                }
                25% {
                    height: 0px;
                    width: 0px;
                    transform: scaleY(0);
                }
                60% {
                    box-shadow: 0 0 10px rgba(0,0,0,0.5);
                    border-radius: 90%;
                }
                100% {
                    width: 80vw;
                    height: 50vh;
                }
                75% {
                    box-shadow: 0 0 150px rgba(229,0,0,0.2);
                }
                80% {
                    box-shadow: 0 0 150px rgba(229,0,0,0.14);
                }
                /*
                85% {
                    box-shadow: 0 0 200px rgba(229,0,0,0.2);
                }
                90% {
                    box-shadow: 0 0 200px rgba(229,0,0,0.14);
                }
                */
                85% {
                    box-shadow: 0 0 150px rgba(47,149,220,0.2);
                }
                90% {
                    box-shadow: 0 0 150px rgba(47,149,220,0.14);
                } 
            
                100% {
                    box-shadow: 0 0 10px rgba(0,0,0,0.5);
                }
            }

            #promo-video{
                width: 100%;
                height: 100%;
            }

            .description{
                margin: auto;
                font-weight: 300;

                width: 840px;
                width: 80vw;
                max-width: 840px;
                min-width: 320px;           
                font-size: 1.10rem;     
                text-align: center;
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">
            <div class="content">
               <div class="m-b-md">
                </div>
                <div class="promo-video box-shadow">
                    <div id="promo-video"></div>
                </div>
                <div class="title m-b-md">
                    <img src="/images/realgate-splash-1.png">
                </div>

                <div class="title m-b-md">

                </div>
                
                <div class="m-b-md">
                </div>
                <div class="links">
                    <a target="blank" href="https://play.google.com/store/apps/details?id=menrise.realgate">
                        <img src="/images/google_play.png">
                    </a>
                    <img onclick="alert('Скоро станет доступно для IOS.');" src="/images/apple_store.png">
                </div>

                <div class="m-b-md">
                </div>

                <div class="description">
                    <strong>Сервис быстрых знакомств вживую</strong>
                    <br><br>
                    Вы выбираете место и приходите в нужное время. Знакомства проходят один на один по 5 минут каждое, в публичном месте. 

                    После всех знакомств можно выбрать понравившихся людей и при взаимной симпатии вы увидите контакты друг друга.
                    <br><br>
    Популярный тип знакомств Speed dating теперь полностью автоматизирован для вашего удобства.
                    <br><br>
                    Типы встреч: 3 пары, 1 на 1 и другие.
                    <br><br>
                    <div class="m-b-md">
                    </div>
                    <strong>Цель проекта</strong>
                    <br><br>
                    С развитием IT мы всё больше погружаемся в окружение вебсайтов и мобильных придожений. Интернет открывает нам весь мир, но запирает нас наедине с нашим устройством.

                    Наша цель это вернуть живое общение, которого так не хватает современным людям.

                    <div class="m-b-md">
                    </div>
                    <strong>Яркий штрих в серых буднях</strong>
                    <br><br>
                    Хотите новых эмоций и впечатлений, быстрые знакомства это тот самый штрих который разукрасит серые будни. Вы встретитесь с новыми, интересными людьми, каждый из которых со своей харизмой и индивидуальностью. Вы можете стать хорошими друзьями или найти вторую половинку, в любом случае вы весело проведёте время. Пробуйте!
                </div>

                <div class="m-b-md">
                </div>

                <div class="m-b-md menu">
                    <a target="blank" href="/license">пользовательское соглашение</a>
                    <a target="blank" href="/rules">правила</a>
                    <a target="blank" href="/privacy-policy">политика конфиденциальности</a>
                    <a target="blank" href="/contacts">контакты</a>
                </div>                              
            </div>
        </div>

    <script>
        // This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');

        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        var player;
        // This function creates an <iframe> (and YouTube player)
        // after the API code downloads.
        setTimeout(function() {
            

            window.player = new YT.Player('promo-video', {
                //height: '0',
                //width: '0',
                videoId: 'xSz98c1Ei60',
                events: {
                    'onReady': onPlayerReady,
                    //'onStateChange': onPlayerStateChange
                }
            });

        }, 4000);

        function onPlayerReady(event) {

            event.target.playVideo();
            setTimeout(function() {
                event.target.playVideo(); //second call need for autoplay without click on window
            }, 100);
        }
        
    </script>


    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
       (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
       m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
       (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

       ym(52836709, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
       });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/52836709" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    </body>
</html>
