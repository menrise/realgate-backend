<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Пользовательское соглашение</title>

        <!-- Styles -->
        <style>
            html, body {
                font-weight: 200;
                color: #636b6f;
                background-color: #fff;
                font-size: 14px;
                font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif; padding:1em;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .content {
                text-align: left;
                max-width: 800px;
            }

            .title {
                font-size: 1.2rem;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>

        <div class="flex-center position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    <strong>Пользовательское соглашение</strong>
                </div>
                
                <p> 
                    Данное пользовательское соглашение разрешает лицам, получившим копию этого программного обеспечения, использовать его, предусмотренным правообладателем способом.
                </p>

                <p> 
                    Данное программного обеспечения предназначено для использования лицами достигшими совершеннолетнего возраста. Используя это программное обеспечение вы подтверждаете что достигли совершеннолетия.
                </p>     

                <p> 
                    Используя это программное обеспечение вы обязуетесь соблюдать <a target="blank" href="/rules">правила пользования сервисом</a>. При нарушении <a target="blank" href="/rules">правил пользования сервисом</a> администрация оставляет за собой право прекратить нарушителю доступ к сервису. 
                </p>

                <p>
                    ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА УЩЕРБ ИЛИ ПО ИНЫМ ТРЕБОВАНИЯМ, В ТОМ ЧИСЛЕ, ПРИ ДЕЙСТВИИ КОНТРАКТА, ДЕЛИКТЕ ИЛИ ИНОЙ СИТУАЦИИ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
                </p>
                
                <p>
                    Пользовательское соглашение может быть изменено путём обновления его текста на нашем портале. Новое пользовательское соглашение вступает в силу сразу после появления его текста на нашем портале.
                </p> 

                <p>
                    Используя данное программное обеспечение вы полностью принимаете это пользовательское соглашение.
                </p>

            </div>
        </div>


        <!-- Yandex.Metrika counter -->
        <script type="text/javascript" >
           (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
           m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
           (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

           ym(52836709, "init", {
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
           });
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/52836709" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
    </body>
</html>
