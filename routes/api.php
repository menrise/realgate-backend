<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Social login routes ...
Route::group(
    ['prefix' => 'auth'], function () {
    Route::any('/{provider}', 'Auth\SocialiteController@redirectToProvider')->name('login')
        ->where('provider', 'google|facebook|vkontakte|instagram');
    Route::any('/{provider}/callback', 'Auth\SocialiteController@handleProviderCallback')
        ->where('provider', 'google|facebook|vkontakte|instagram');
});

Route::group([ 'middleware' => 'jwt' ], function () {//jwt //auth:api //['jwt.auth', 'jwt.refresh']
    Route::put('/users', 'Auth\SocialiteController@updateUser');
    
    Route::post('/profileImages', 'Auth\SocialiteController@uploadProfileImage');
    Route::get('/profileImages/{userId}', 'Auth\SocialiteController@getProfileImages');
    Route::put('/profileImage/{imageId}/setMain', 'Auth\SocialiteController@setMainProfileImage');
    Route::delete('/profileImage/{imageId}', 'Auth\SocialiteController@deleteProfileImage');

    Route::put('/meets', 'Meet\MeetController@update');
    //its gets meet place json list. Used - POST method because I send big composite_meet_ids list as parameter
    Route::post('/meets', 'Meet\MeetController@index');

    Route::post('/adventures', 'Adventure\AdventureController@store');
    Route::put('/adventures/{adventure}', 'Adventure\AdventureController@update');
    Route::get('/adventures/near/{latitude}/{longitude}', 'Adventure\AdventureController@getNearAdventures');
    Route::get('/adventures/my_own', 'Adventure\AdventureController@getMyOwnAdventures');
    Route::get('/adventures/my_participation',
        'Adventure\AdventureController@getMyParticipationAdventures');

    Route::get('/adventures/{adventure}/users', 'Adventure\AdventureController@getAdventuresUsers');
    Route::post('/adventures/{adventure}/user', 'Adventure\AdventureController@postAdventuresUser');
    Route::delete('/adventures/{adventure}/user/{user}', 'Adventure\AdventureController@deleteAdventuresUser');
    Route::put('/adventures/{adventure}/users/approve', 'Adventure\AdventureController@approveUsers');

    Route::post('/chat/private_message/{receiverUserId}', 'Chat\PrivateMessageController@postPrivateMessage');
    Route::get('/chat/private_messages/contacts/', 'Chat\PrivateMessageController@getContacts');
    Route::get('/chat/private_messages/{secondUserId}', 'Chat\PrivateMessageController@getPrivateMessages');
    Route::put('/chat/private_messages/{messageId}/read', 'Chat\PrivateMessageController@setPrivateMessageReaded');


    Route::put('/likes', 'Meet\LikeController@update');

    Route::put('/users/device_time', 'Auth\SocialiteController@updateUserDeviceTime');
    Route::put('/users/coordinates', 'Auth\SocialiteController@updateUserCoordinates');
    Route::get('/users/{user}', 'Auth\SocialiteController@getUser');

    //vendor package route files alymosul/laravel-exponent-push-notifications/Http/routes.php exluded in package.json
    //this is override alymosul/laravel-exponent-push-notifications/Http/routes.php
    //for suit middleware
	Route::group(['prefix' => 'api/exponent/devices', 'middleware' => ['bindings']], function () { 
	    Route::post('subscribe', [
	        'as'    =>  'register-interest',
	        'uses'  =>  '\NotificationChannels\ExpoPushNotifications\Http\ExpoController@subscribe',
	    ]);

	    Route::post('unsubscribe', [
	        'as'    =>  'remove-interest',
	        'uses'  =>  '\NotificationChannels\ExpoPushNotifications\Http\ExpoController@unsubscribe',
	    ]);
	});

});
