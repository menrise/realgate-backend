<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Page\PageController@welcome');

Route::get('/privacy-policy', 'Page\PageController@privacy_policy');

Route::get('/license', 'Page\PageController@license');

Route::get('/rules', 'Page\PageController@rules');

Route::get('/contacts', 'Page\PageController@contacts');