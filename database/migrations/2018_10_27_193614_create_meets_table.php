<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('google_place_id');
            $table->dateTime('meet_time');
            $table->integer('user_group')->unsigned();
            $table->string('composite_meet_id')->comment = "concatenation: meet_time, user_group, google_place_id";
            $table->unique(['google_place_id', 'meet_time', 'user_group']);

            $table->index('user_group');
            $table->foreign('user_group')->references('id')->on('user_groups');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meets');
    }
}
