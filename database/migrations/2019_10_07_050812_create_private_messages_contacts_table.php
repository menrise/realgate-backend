<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivateMessagesContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('private_messages_contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_1_id');
            $table->unsignedInteger('user_2_id');
            $table->unsignedInteger('user_1_received_messages')->default(0);
            $table->unsignedInteger('user_2_received_messages')->default(0);
            $table->unsignedInteger('user_1_received_messages_before_read')->default(0);
            $table->unsignedInteger('user_2_received_messages_before_read')->default(0);    
            $table->unsignedInteger('user_1_last_readed_mes_id')->nullable();
            $table->unsignedInteger('user_2_last_readed_mes_id')->nullable(); 
            $table->unsignedInteger('last_mes_id')->nullable();
       

            $table->unique(['user_1_id', 'user_2_id']);
            $table->foreign('user_1_id')->references('id')->on('users');   
            $table->foreign('user_2_id')->references('id')->on('users');             
            $table->foreign('user_1_last_readed_mes_id')->references('id')->on('private_messages')->onDelete('set null');
            $table->foreign('user_2_last_readed_mes_id')->references('id')->on('private_messages')->onDelete('set null');
            $table->foreign('last_mes_id')->references('id')->on('private_messages')->onDelete('set null');                 

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('private_messages_contacts');
    }
}
