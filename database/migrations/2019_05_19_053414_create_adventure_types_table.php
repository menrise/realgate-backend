<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdventureTypesTable extends Migration
{

    private $table = 'adventure_types';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('font_type');
            $table->string('icon_name');            
            $table->timestamps();
        });

        DB::table($this->table)->insert(
            [
                [
                    'id' => 1,
                    'title' => 'В кино',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'film',
                ],
                [
                    'id' => 2,
                    'title' => 'В ресторан',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'coffee',                    
                ],
                [
                    'id' => 3,
                    'title' => 'Прогулка',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'walking',                     
                ],
                [
                    'id' => 4,
                    'title' => 'Покататься на машине',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'car',                     
                ],
                [
                    'id' => 5,
                    'title' => 'Пройти квест, автоквест, тематическую игру',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'fort-awesome',                     
                ],
                [
                    'id' => 6,
                    'title' => 'Сыграть в настольную или психологическую игру',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'brain',                     
                ],
                [
                    'id' => 7,
                    'title' => 'Покататься на велосипедах',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'bicycle', 
                ],
                [
                    'id' => 8,
                    'title' => 'Покататься на мотобайках',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'motorcycle',                     
                ],
                [
                    'id' => 9,
                    'title' => 'Совместный спорт',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'dumbbell',                     
                ],          
                [
                    'id' => 10,
                    'title' => 'Совместный шоппинг',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'shopping-cart',                     
                ],   
                [
                    'id' => 11,
                    'title' => 'В ночной клуб, на концерт',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'angellist',                     
                ],   
                [
                    'id' => 12,
                    'title' => 'В бар, паб',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'cocktail',                     
                ],
                [
                    'id' => 13,
                    'title' => 'Покататься на сноуборде, горных лыжах',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'snowboarding',                     
                ],
                [
                    'id' => 14,
                    'title' => 'Покататься на коньках',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'skating',                     
                ],
                [
                    'id' => 15,
                    'title' => 'На атракционы, в парк развлечений',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'hat-wizard',                     
                ],
                [
                    'id' => 16,
                    'title' => 'Совместное путешествие',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'plane',                     
                ],
                [
                    'id' => 17,
                    'title' => 'Прогулка на яхте, катере',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'ship',                     
                ],
                [
                    'id' => 18,
                    'title' => 'Отдых на природе',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'campground',                     
                ],
                [
                    'id' => 19,
                    'title' => 'Загородный отдых, коттедж',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'home',                     
                ],
                [
                    'id' => 20,
                    'title' => 'Домашняя вечеринка',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'building',                     
                ],
                [
                    'id' => 21,
                    'title' => 'В аквапарк, бассейн',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'swimmer',                     
                ],
                [
                    'id' => 22,
                    'title' => 'В театр, музей, на экскурсию',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'snowboarding',                     
                ],
                [
                    'id' => 23,
                    'title' => 'На футбол, хоккей, соревнования',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'futbol-o',                     
                ],
                [
                    'id' => 24,
                    'title' => 'На массовое мероприятие, праздник',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'archway',                     
                ],
                [
                    'id' => 25,
                    'title' => 'На рыбалку, охоту, в поход',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'hiking',                     
                ],
                [
                    'id' => 26,
                    'title' => 'Подымить кальяном',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'smog',                     
                ],
                [
                    'id' => 27,
                    'title' => 'Сыграть в бильярд, боулинг',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'bowling-ball',                     
                ],
                [
                    'id' => 28,
                    'title' => 'Участие в пейнтболе, страйкболе, посетить тир',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'bullseye',                     
                ],
                [
                    'id' => 29,
                    'title' => 'Сходить в батутный зал, скалодром',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'user-injured',                     
                ],
                [
                    'id' => 30,
                    'title' => 'Прыгнуть с банджи, парашутом',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'parachute-box',                     
                ],  
                [
                    'id' => 31,
                    'title' => 'За шавухой',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'hamburger',                     
                ], 
                [
                    'id' => 32,
                    'title' => 'Пообщаться',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'comments',                     
                ], 
                [
                    'id' => 33,
                    'title' => 'Другое',
                    'font_type' => 'font-awesome',
                    'icon_name' => 'question',                     
                ],                    
            ]
        );        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
