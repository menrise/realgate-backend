<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CanceledMeetMemberings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('canceled_meet_memberings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meet_id');
            $table->unsignedInteger('user_id');
            $table->boolean('not_enough_members')->nullable()->comment = "true - not enough members, false - other reson";
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('canceled_meet_memberings');
    }
}
