<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdventuresTable extends Migration
{   
    private $table = 'adventures';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            
            $table->unsignedInteger('owner_id')->index();
            $table->unsignedInteger('adventure_type_id')->index();
            $table->unsignedInteger('adventure_spend_id')->index();
            $table->unsignedInteger('adventure_transfer_id')->index();
            $table->unsignedInteger('adventure_relation_id')->index();

            $table->string('name');
            $table->string('description')->default('');
            $table->dateTime('time')->nullable();
            $table->boolean('time_by_agreement')->default(true);
            
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('adventure_type_id')->references('id')->on('adventure_types');
            $table->foreign('adventure_spend_id')->references('id')->on('adventure_spends');
            $table->foreign('adventure_transfer_id')->references('id')->on('adventure_relations');
            $table->foreign('adventure_relation_id')->references('id')->on('adventure_transfers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
