<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdventureTransfersTable extends Migration
{   
    private $table = 'adventure_transfers';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->timestamps();
        });

        DB::table($this->table)->insert(
            [
                [
                    'id' => 1,
                    'title' => 'Не указано',
                ],
                [
                    'id' => 2,
                    'title' => 'Привезу, отвезу если нужно',
                ],
                [
                    'id' => 3,
                    'title' => 'Нужно добираться к месту самостоятельно',
                ],                   
            ]
        );         
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
