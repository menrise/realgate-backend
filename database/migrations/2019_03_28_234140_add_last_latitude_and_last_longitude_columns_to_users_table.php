<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLastLatitudeAndLastLongitudeColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('latitude', 17, 14)->default('0')->index();
            $table->float('longitude', 17, 14)->default('0')->index();
            $table->unsignedTinyInteger('get_notice_near_meet')->default(1)->index();
            $table->dateTime('notice_near_meet_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('get_notice_near_meet');
            $table->dropColumn('notice_near_meet_at');
        });
    }
}
