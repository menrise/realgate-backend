<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserGroupsTable extends Migration
{


    private $table = 'user_groups';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create($this->table, function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('sex_composition_id')->nullable()->comment = "1 - poly sex (man and woman), 2 mono sex (man), 3 mono sex (woman)";
            $table->char('description', 50)->nullable();
            $table->unsignedTinyInteger('max_mans')->default(0);
            $table->unsignedTinyInteger('max_womans')->default(0);
            $table->unsignedTinyInteger('mix_age')->default(0);
            $table->unsignedInteger('max_age')->default(0);
            $table->timestamps();
        });

        
        DB::table($this->table)->insert(
            [
                [
                    'id' => 1,
                    'sex_composition_id' => 1,
                    'description' => 'Младше 19',
                    'max_mans' => 3,
                    'max_womans' => 3,
                    'mix_age' => 0,
                    'max_age' => 18,
                ],
                [
                    'id' => 2,
                    'sex_composition_id' => 1,
                    'description' => '19 - 35 лет',
                    'max_mans' => 3,
                    'max_womans' => 3,
                    'mix_age' => 19,
                    'max_age' => 35,
                ],
                [
                    'id' => 3,
                    'sex_composition_id' => 1,
                    'description' => '35+ лет',
                    'max_mans' => 3,
                    'max_womans' => 3,
                    'mix_age' => 36,
                    'max_age' => 300,
                ],
                [
                    'id' => 4,
                    'sex_composition_id' => 2,
                    'description' => 'Только М, младше 19',
                    'max_mans' => 4,
                    'max_womans' => 0,
                    'mix_age' => 0,
                    'max_age' => 18,
                ],
                [
                    'id' => 5,
                    'sex_composition_id' => 2,
                    'description' => 'Только М, 19 - 35 лет',
                    'max_mans' => 4,
                    'max_womans' => 0,
                    'mix_age' => 19,
                    'max_age' => 35,
                ],
                [
                    'id' => 6,
                    'sex_composition_id' => 2,
                    'description' => 'Только М, 35+ лет',
                    'max_mans' => 4,
                    'max_womans' => 0,
                    'mix_age' => 36,
                    'max_age' => 300,                    
                ],
                [
                    'id' => 7,
                    'sex_composition_id' => 3,
                    'description' => 'Только Ж, младше 19',
                    'max_mans' => 0,
                    'max_womans' => 4,
                    'mix_age' => 0,
                    'max_age' => 18,
                ],

                [
                    'id' => 8,
                    'sex_composition_id' => 3,
                    'description' => 'Только Ж, 19 - 35 лет',
                    'max_mans' => 0,
                    'max_womans' => 4,
                    'mix_age' => 19,
                    'max_age' => 35,
                ],
                [
                    'id' => 9,
                    'sex_composition_id' => 3,
                    'description' => 'Только Ж, 35+ лет',
                    'max_mans' => 0,
                    'max_womans' => 4,
                    'mix_age' => 36,
                    'max_age' => 300, 
                ],                
            ]
        );
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
