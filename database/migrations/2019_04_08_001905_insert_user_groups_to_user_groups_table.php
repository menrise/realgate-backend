<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertUserGroupsToUserGroupsTable extends Migration
{
    private $table = 'user_groups';

    private $groups =  [
        [
            'id' => 10,
            'sex_composition_id' => 1,
            'description' => '1 на 1, Младше 19',
            'max_mans' => 1,
            'max_womans' => 1,
            'min_age' => 0,
            'max_age' => 18,
        ],
        [
            'id' => 11,
            'sex_composition_id' => 1,
            'description' => '1 на 1, 19 - 35 лет',
            'max_mans' => 1,
            'max_womans' => 1,
            'min_age' => 19,
            'max_age' => 35,
        ],
        [
            'id' => 12,
            'sex_composition_id' => 1,
            'description' => '1 на 1, 35+ лет',
            'max_mans' => 1,
            'max_womans' => 1,
            'min_age' => 36,
            'max_age' => 300,
        ],
        [
            'id' => 13,
            'sex_composition_id' => 2,
            'description' => '1 на 1, Только М, младше 19',
            'max_mans' => 2,
            'max_womans' => 0,
            'min_age' => 0,
            'max_age' => 18,
        ],
        [
            'id' => 14,
            'sex_composition_id' => 2,
            'description' => '1 на 1, Только М, 19 - 35 лет',
            'max_mans' => 2,
            'max_womans' => 0,
            'min_age' => 19,
            'max_age' => 35,
        ],
        [
            'id' => 15,
            'sex_composition_id' => 2,
            'description' => '1 на 1, Только М, 35+ лет',
            'max_mans' => 2,
            'max_womans' => 0,
            'min_age' => 36,
            'max_age' => 300,                    
        ],
        [
            'id' => 16,
            'sex_composition_id' => 3,
            'description' => '1 на 1, Только Ж, младше 19',
            'max_mans' => 0,
            'max_womans' => 2,
            'min_age' => 0,
            'max_age' => 18,
        ],
        [
            'id' => 17,
            'sex_composition_id' => 3,
            'description' => '1 на 1, Только Ж, 19 - 35 лет',
            'max_mans' => 0,
            'max_womans' => 2,
            'min_age' => 19,
            'max_age' => 35,
        ],
        [
            'id' => 18,
            'sex_composition_id' => 3,
            'description' => '1 на 1, Только Ж, 35+ лет',
            'max_mans' => 0,
            'max_womans' => 2,
            'min_age' => 36,
            'max_age' => 300, 
        ],                
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table($this->table)->insert(
            $this->groups
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $ids_to_delete = array_map(function($item){ return $item['id']; }, $this->groups);
        DB::table($this->table)->whereIn('id', $ids_to_delete)->delete();
    }
}
