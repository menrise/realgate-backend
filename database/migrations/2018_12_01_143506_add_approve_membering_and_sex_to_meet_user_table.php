<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApproveMemberingAndSexToMeetUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meet_user', function (Blueprint $table) {
            $table->boolean('approve_membering')->nullable()->comment = "true - approve membering, false - cancel membering, null - waiting for decide";
            $table->unsignedTinyInteger('sex')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meet_user', function (Blueprint $table) {
            $table->dropColumn('approve_membering');
            $table->dropColumn('sex');
        });
    }
}
