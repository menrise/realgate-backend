<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdventureUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adventure_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('adventure_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->datetime('approve_membering_at')->nullable();
            $table->boolean('approve_membering')->nullable()->comment = "true - approve membering, false - not approve membering, null - not decided";
            $table->unsignedTinyInteger('sex')->nullable();

            $table->unique(['adventure_id', 'user_id']);

            $table->foreign('adventure_id')->references('id')->on('adventures')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adventure_user');
    }
}
